window.WorkGallery = function() {
  function containerGridMasonry($root) {
    var $gridContainer = $root.find('.grid'),
        $categoriesFilter = $root.find('.x-categories-filter');

    $gridContainer.imagesLoaded(function() {
      $gridContainer.isotope({
        itemSelector: '.grid-item'
      });
    });

    $categoriesFilter.on('click', '.categories', function(e) {
      e.preventDefault();

      $categoriesFilter.find('.active').removeClass('active');
      $(this).addClass('active');

      var filterValue = $(this).attr('data-filter');
      $gridContainer.isotope({ filter: filterValue });
    });
  };

  function setup() {
    var $root = $('#work-gallery');
    containerGridMasonry($root);
  }

  return {
    setup: setup
  }
}()
