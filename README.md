# One page site requirements

Sample sites:

* http://mj1388massage.com/
* http://coscofloor.com/
* http://1979collection.com/
* http://pengchengtravel.com/
* http://djbeauty.net/

Please take a look at the `sample sites` folder

## Sections

It consists of a top nav, a simple footer, and no more than 5 sections.

Each section can be

* an intro landing section like we see the first section from all sites above
  - see the `resources` folder for some sample js and html code
* a Masonry image collage where each image is clickable onto a popup. See the
  services section on http://coscofloor.com/
  - resource: http://masonry.desandro.com/
* an image slider (owlcarousel.owlgraphic.com/demos/demos.html)
* an about section like the 3rd section on http://pengchengtravel.com/
* a contact info section with map like we see at bottom of http://mj1388massage.com/
* an info section contains like the "About Us" section from http://mj1388massage.com/

If you already have sections similar to this, you can use your own. As long as
we can have the content and images on the site.

## Resources

You can use fontawesome icons and any other ones. We do have an `resources/icons`
folder that contains tons of svg icons for you.
